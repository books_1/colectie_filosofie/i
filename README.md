# I

## Content

```
./Iancu Lucica:
Iancu Lucica - Concepte si metode matematice in logica.pdf
Iancu Lucica - Ex falso quodlibet.pdf
Iancu Lucica - Logica formala.pdf
Iancu Lucica - Logica generala vol. 1.pdf
Iancu Lucica - Logica modala.pdf
Iancu Lucica - Metalogica, vol. I.pdf

./Ian Stewart:
Ian Stewart - Imblanzirea infinitului (Povestea matematicii).pdf
Ian Stewart - Numerele naturii.pdf

./Ilie Parvu:
Ilie Parvu - Arhitectura existentei, vol. 2.pdf
Ilie Parvu - Arhitectura existentei vol. I (Paradigma structural generativa in ontologie).pdf
Ilie Parvu - Arhitectura existentei vol. II (Teoria elementelor versus Structura categoriala a lumii).pdf
Ilie Parvu - Cum se interpreteaza operele filosofice.pdf
Ilie Parvu - Introducere in epistemologie.pdf
Ilie Parvu - Posibilitatea experientei.pdf
Ilie Parvu - Semantica si logica stiintei.pdf
Ilie Parvu - Teoria stiintifica.pdf

./Ilya Prigogine & Isabelle Stengers:
Ilya Prigogine & Isabelle Stengers - Noua alianta, metamorfoza stiintei.pdf

./Immanuel Kant:
Immanuel Kant - Antropologia din perspectiva pragmatica.pdf
Immanuel Kant - Critica facultatii de judecare.pdf
Immanuel Kant - Critica ratiunii practice.pdf
Immanuel Kant - Critica ratiunii pure.pdf
Immanuel Kant - Despre Pedagogie.pdf
Immanuel Kant - Intemeierea metafizicii moravurilor (Humanitas).pdf
Immanuel Kant - Logica generala.pdf
Immanuel Kant - Observatii asupra sentimentului de frumos si sublim.pdf
Immanuel Kant - Prolegomene.pdf
Immanuel Kant - Religia doar in limitele ratiunii.pdf
Immanuel Kant - Spre pacea eterna.pdf

./Ioan C. Ivanciu:
Ioan C. Ivanciu - Filosofia istoriei.pdf

./Ioan Deac:
Ioan Deac - Principiile metafizicii carteziene.pdf

./Ioan Hirghidus:
Ioan Hirghidus - Epistemologie.pdf

./Ioan N. Rosca:
Ioan N. Rosca - Filosofia moderna (Editia II).pdf
Ioan N. Rosca - Filosofie moderna (Editia III).pdf
Ioan N. Rosca - Introducere in axiologie.pdf

./Ioan Petru Culianu:
Ioan Petru Culianu - Arborele Gnozei.pdf
Ioan Petru Culianu - Cult, magie, erezii.pdf
Ioan Petru Culianu - Eros si magie in Renastere (1484).pdf
Ioan Petru Culianu - Experiente ale extazului.pdf
Ioan Petru Culianu - Gnosticism si gindire moderna (Hans Jonas).pdf
Ioan Petru Culianu - Gnozele dualiste ale Occidentului.pdf
Ioan Petru Culianu - Iocari Serio.pdf
Ioan Petru Culianu - Iter in silvis.pdf
Ioan Petru Culianu - Jocurile Mintii.pdf
Ioan Petru Culianu - Omul si opera.pdf
Ioan Petru Culianu - Studii romanesti, vol. 1.pdf

./Ioan Petru Culianu & Mircea Eliade:
Ioan Petru Culianu & Mircea Eliade - Dictionar al religiilor.pdf

./Ioan Scottus Eriugena:
Ioan Scottus Eriugena - Comentariu la Evanghelia lui Ioan.pdf

./Ion Banu:
Ion Banu - Filosofia greaca pana la Platon, vol. II partea I.pdf
Ion Banu - Filosofia greaca pana la Platon, vol. II partea II.pdf
Ion Banu - Filosofia greaca pana la Platon, vol. I partea I.pdf
Ion Banu - Filosofia greaca pana la Platon, vol. I partea II.pdf

./Ion Copoeru:
Ion Copoeru - Aparenta si sens.pdf

./Ionel Narita:
Ionel Narita - Analiza logica.pdf
Ionel Narita - Logica simbolica.pdf

./Ion Petrovici:
Ion Petrovici - Schopenahauer, viata si opera filosofica.pdf
Ion Petrovici - Teoria notiunilor.pdf
Ion Petrovici - Viata si opera lui Kant.pdf

./Ion Popescu:
Ion Popescu - Corabia lui Tezeu.pdf

./Ion Tudosescu:
Ion Tudosescu - Actiunea sociala eficienta.pdf
Ion Tudosescu - Dialectica actuala a vietii sociale si sensul istoriei.pdf
Ion Tudosescu - Filosofia si conditia umana.pdf

./Ionut Stefan:
Ionut Stefan - Aristotel si presocraticii.pdf

./Ion Vitner:
Ion Vitner - Albert Camus sau tragicul exilului.pdf

./Isaac Newton:
Isaac Newton - Optica.pdf
Isaac Newton - Principiile matematice ale filozofiei naturale.pdf

./Isaiah Berlin:
Isaiah Berlin - Patru eseuri despre libertate.pdf
Isaiah Berlin - Puterea ideilor.pdf
Isaiah Berlin - Simtul realitatii.pdf

./Ivo Frenzel:
Ivo Frenzel - Friedrich Nietzsche.pdf
```

